import DS from 'ember-data';

var Model = DS.Model.extend({
  type: DS.attr('string'),
  week: DS.belongsTo('week'),
  workouts: DS.hasMany('workout', {async: true})
});

export default Model;